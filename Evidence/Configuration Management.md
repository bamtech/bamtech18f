### Configuration Management ###

A publicly available Git repository hosted on Bitbucket was utilized as our primary means for revision and source control. 

Our TeamCity builds and deployments utilized the 'Master' branch as this was a prototype.

![myfda-teamcity.PNG](https://bitbucket.org/repo/6d4k9y/images/3013123199-myfda-teamcity.PNG)