Initial design decisions for MyFDA were inspired by SB Admin 2 BootStrap theme available on StartBootStrap.com

![sb-admin-2.jpg](https://bitbucket.org/repo/6d4k9y/images/2376236542-sb-admin-2.jpg)

Figure 1: SB Admin Bootstrap Theme (fully responsive)

The team discussed potential layout and user interaction through a series of drawings. The two ideal candidates were concepts to build out the UI

![Mockup - Landing Page.PNG](https://bitbucket.org/repo/6d4k9y/images/479062647-Mockup%20-%20Landing%20Page.PNG)

Figure 2: Mock-up of Landing Page using design features of SB Admin

![Mockup - View Recall Report.PNG](https://bitbucket.org/repo/6d4k9y/images/2176441839-Mockup%20-%20View%20Recall%20Report.PNG)

Figure 3: Mock-up of Food Recall View

UX Design Considerations - How do we present FDA data with meaning and put it into context for the user?

- Organize recall data in a manner that makes sense. 
  - Identity Classification and Status upfront
  - Clearly display the production description and reason for recall
  - Display Event Details
  - Be transparent. Was this a mandatory or voluntary enforcement report?

