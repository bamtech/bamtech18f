### Continuous Monitoring ###
The Windows Azure monitoring was used to perform continuous monitoring
To protect Azure platform services, Microsoft provides a distributed denial-ofservice
(DDoS) defense system that is part of Azure’s continuous monitoring
process, and is continually improved through penetration-testing. Azure’s DDoS
defense system is designed to not only withstand attacks from the outside, but
also from other Azure tenants:
1. Network-layer high volume attacks. These attacks choke network pipes and
packet processing capabilities by flooding the network with packets. The
Azure DDoS defense technology provides detection and mitigation
techniques such as SYN cookies, rate limiting, and connection limits to help
ensure that such attacks do not impact customer environments.

2. Application-layer attacks. These attacks can be launched against a customer
VM. Azure does not provide mitigation or actively block network traffic
affecting individual customer deployments, because the infrastructure does
not interpret the expected behavior of customer applications. In this case,
similar to on-premises deployments, mitigations include:

* Running multiple VM instances behind a load-balanced Public IP
address.
* Using firewall proxy devices such as Web Application Firewalls (WAFs)
that terminate and forward traffic to endpoints running in a VM. This
provides some protection against a broad range of DoS and other
attacks, such as low-rate, HTTP, and other application-layer threats.

![ContinuousMonitoringUpdated.PNG](https://bitbucket.org/repo/6d4k9y/images/1860239641-ContinuousMonitoringUpdated.PNG)