### Continuous Integration Details ###
**TeamCity** is a Java-based build management and continuous integration server from JetBrains. It was first released on October 2, 2006.[1] TeamCity is commercial software and licensed under a proprietary license. A Freemium license for up to 20 build configurations and 3 free Build Agent licenses is available. Open Source projects can request a free license.

### We utilize team city to build, test and deploy our application. ###

![myfda-teamcity.PNG](https://bitbucket.org/repo/6d4k9y/images/3013123199-myfda-teamcity.PNG)