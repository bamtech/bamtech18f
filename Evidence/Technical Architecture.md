## MyFDA has been built and deployed using the following frameworks and components ##

MyFDA was built using Microsoft Visual Studio 2015 Release Candidate, ASP.NET 5 vNext, C#, and MVC 6. BAM Technologies chose this approach to showcase Microsoft .NET’s upcoming capability to be cross-platform. In our project structure we used tools such as Bower and Gulp to handle pre-processing and packaging, which is natively supported in MVC 6 Application project. We leveraged additional open-source frameworks and libraries such as jQuery, C3.js a D3 based chart library, BootStrap for UI, AutoFac as our inversion of control container, MediatR to implement in-process messaging.

![pasted_image_at_2015_06_22_10_26_pm.png](https://bitbucket.org/repo/6d4k9y/images/3344725450-pasted_image_at_2015_06_22_10_26_pm.png)


### **Build** ###
1. **AutoFac** - Inversion of Control container
2. **C3.js** - D3-based reusable chart library
3. **ASP.NET 5** - (ASPvNext) ASP.NET 5 is a lean and composable framework for building web and cloud applications.    ASP.NET 5 is fully open source and available on GitHub
4. **Bootstrap** - Responsive HTML5/CSS3 framework
5. **Bower** - Package manager
6. **JQuery** - is a fast, small, and feature-rich JavaScript library
7. **Gulp.js** - streaming build system 
 
 

### **Deploy** ###
* [TeamCity ](https://www.jetbrains.com/teamcity/)- Continuous Integration 
* [Ubuntu Server 14.04 LTS](http://www.ubuntu.com/server) - Web Server
* [Docker ](https://www.docker.com/)- Container Distribution
* [Windows Azure](https://azure.microsoft.com/en-us/) - Hosting Environment (Infrastructure as a Service)
* [Microsoft Azure Configuration Manager 3.1.0](https://www.nuget.org/packages/Microsoft.WindowsAzure.ConfigurationManager/) - Configuration Management

 

