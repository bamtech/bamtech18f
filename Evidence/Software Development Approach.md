Software Development Details - Scrum Methodology 

Scrum is an iterative and incremental agile software development methodology for managing product development. It defines "a flexible, holistic product development strategy where a development team works as a unit to reach a common goal", challenges assumptions of the "traditional, sequential approach" to product development, and enables teams to self-organize by encouraging physical co-location or close online collaboration of all team members, as well as daily face-to-face communication among all team members and disciplines in the project.



![ScrumTeam-18F.png](https://bitbucket.org/repo/6d4k9y/images/2291530110-ScrumTeam-18F.png)