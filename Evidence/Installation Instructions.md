### Installation Steps for MyFDA###

### Technical Requirements ###
**Server** - Ubuntu Linux Server 
**Container** - Docker 
**Hosting Environment** - Windows Azure - Hosting Environment (Infrastructure as a Service)

1. Download and install Ubuntu Server 14.04 in your Cloud Hosting environment http://www.ubuntu.com/server
2. Clone MyFDA BitBucket repo https://bitbucket.org/bamtech/bamtech18f
3. Configure a custom docker container for Ubuntu Linux
4. Use the MyFDA docker files found in \src\OpenFda\Properties\PublishProfiles to configure docker settings
5. Build and deploy MyFDA application  

Step-by-Step instructions using Windows Azure and Microsoft Visual Studio 2015

https://msdn.microsoft.com/en-US/library/azure/mt125409.aspx 


### Steps for installing and running MyFda via command line###
1) Clone the repository to a local directory

2) Install dnvm by running the following in a command line window:

```
#!cmd

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "&{$Branch='dev';iex ((new-object net.webclient).DownloadString('https://raw.githubusercontent.com/aspnet/Home/dev/dnvminstall.ps1'))}"
```

3) Ensure that dnvm has been added to your path, if not you may need to log out and log back in.
4) Install the appropriate dnx runtime by running the following in a command line window:
```
#!cmd
dnvm install 1.0.0-beta4 -architecture x64 -runtime coreclr
```
5) Open the file located at %AppData%\NuGet\NuGet.config, and replace its contents with the following

```
#!xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageSources>
    <add key="AspNetVNext" value="https://www.myget.org/F/aspnetvnext/api/v2/" />
    <add key="nuget.org" value="https://www.nuget.org/api/v2/" />
  </packageSources>
  <disabledPackageSources />
  <activePackageSource>
    <add key="nuget.org" value="https://www.nuget.org/api/v2/" />
  </activePackageSource>
</configuration>

```

6) Navigate to the <repo root>\bamtech18f\OpenFda and execute the following in a command line window:
```
#!cmd
dnx . web
```
7) Open a webbrowser and navigate to http://localhost:5000.