# README #

### Submission for POOL TWO: Development Pool ###

### URL to Working Prototype - http://myfda.cloudapp.net/ ###
### Source Code is in the Master branch ###


**(Built using the .NET Core and ASP.NET 5 vNext frameworks)**

![pasted_image_at_2015_06_22_10_26_pm.png](https://bitbucket.org/repo/6d4k9y/images/3344725450-pasted_image_at_2015_06_22_10_26_pm.png)

### Brief Description ###

MyFDA is a modern responsive web interface providing real-time access to the latest food, drug, and device recall and enforcement data leveraging OpenFDA’s open-source APIs. MyFDA provides a search capability, data visualization, and detailed event descriptions. When viewing a specific event, MyFDA provides additional insight for the user. The application mixes in related recalls for the recalling firm as well as additional news alerts that may be related to the event by implementing Google’s news API. The user can perform additional queries by selecting a specific or custom date range when viewing a recall dashboard for each dataset. MyFDA is a user-friendly reporting tool that can be used by individuals, super markets, pharmacies, and small clinics to quickly research products that may have been recalled or reported by the FDA. 

Our team consisted of a Technical Architect, Front-end and Back-end developers, and a DevOps Engineer. Project Management was accomplished using the Visual Studio Online SCRUM project template. We leveraged the Kanban board to actively track and manage our backlog and track bugs for each of our sprints. Daily standups were conducted using Skype for Business and WebEx meetings. Our repository was established using BitBucket.

The vision for MyFDA began with an early discovery for the type of information OpenFDA datasets provided. We reviewed each of the domains and performed a number of queries using Postman Rest Client to analyze the results and determined how the APIs behaved and how we could use the information. Our User-Interface (UI) drew inspiration from a variety of dashboard templates available on WrapBootStrap.com and StartBootStrap.com. We conducted additional peer-reviews internally and solicited some feedback from external stakeholders. Overall, much of the user design was based-off our experiences with other government clients and products we’ve provided them for the last 10 years. 

MyFDA was built using Microsoft Visual Studio 2015 Release Candidate, ASP.NET 5 vNext, C#, and MVC 6. BAM Technologies chose this approach to showcase Microsoft .NET’s upcoming capability to be cross-platform. In our project structure we used tools such as Bower and Gulp to handle pre-processing and packaging, which is natively supported in MVC 6 Application project. We leveraged additional open-source frameworks and libraries such as jQuery, C3.js a D3 based chart library, BootStrap for UI, AutoFac as our inversion of control container, MediatR to implement in-process messaging.

Our overall design and solution approach takes from the Command Query Separation (CQS) design pattern. Although our prototype does not have a persistence mechanism, we leveraged the Query structure of the pattern to simplify calls to the API and exposed strongly-typed queries and result objects to the UI. The MediatR library helps facilitate communications from the controller action to the query handler. The query handler orchestrates an underlying API call using a RestHelper class we developed to query the OpenFDA dataset and parse the JSON to a result object. Our implementation uses a few secure software design practices to prevent over and under posting attacks and cross-site forgery requests.

Our team used JetBrains Team City to implement continuous integration and deployment to Microsoft Azure. This was accomplished by pointing Team City to our BitBucket repository, which would detect changes, run tests, and then perform builds. The binaries were packaged into a Docker container and deployed to a Ubuntu Server 14.04 LTS virtualized machines. Continuous monitoring is tracked using Azure Portal platform and NewRelic web-browser application insight data.

### Team     **(EVIDENCE CRITERIA #b)**
* @gbarborak - Team Leader/Technical Architect **(EVIDENCE CRITERIA #a)**
* @mujahid_ali - Frontend Developer
* @justindneal - Backend Developer
* @rchange - Product Manager/DevOps Engineer

### Open-Sourced Technologies Used  **(EVIDENCE CRITERIA #c)**
1. [AutoFac](http://autofac.org/) - Inversion of Control container
2. [C3.js](http://c3js.org/) - D3-based reusable chart library
3. [ASP.NET 5](http://www.asp.net/vnext) - (ASPvNext) ASP.NET 5 is a lean and composable framework for building web and cloud applications. ASP.NET 5 is fully open source and available on GitHub
4. [Bootstrap](getbootstrap.com) - Responsive HTML5/CSS3 framework
5. [Bower](http://bower.io/) - Package manager
6. [JQuery](https://jquery.com/) - is a fast, small, and feature-rich JavaScript library
7. [Gulp.js](http://gulpjs.com/) - streaming build system

### Deployed On [Windows Azure](https://azure.microsoft.com/en-us/) **(EVIDENCE CRITERIA #d)**

### Unit Tests **(EVIDENCE CRITERIA #e)**
Test project can be found in [Source](https://bitbucket.org/bamtech/bamtech18f/src) the [OpenFDA.Test](https://bitbucket.org/bamtech/bamtech18f/src/777cb26daf6527c7e9551bb4d9099d5bbb40d1ac/OpenFda/src/OpenFda.Test/?at=master) project

### Continuous Integration **(EVIDENCE CRITERIA #f)** 
This project utilized [TeamCity](https://www.jetbrains.com/teamcity/) for Continuous Integration (see Evidence - Continuous Integration) 

### Configuration Management **(EVIDENCE CRITERIA #g)** 
Our Bitbucket repository was used for version control and TeamCity builds (see Evidence - Configuration Management) 

### Continuous Monitoring **(EVIDENCE CRITERIA #h)** 
The Windows Azure monitoring was used to perform continuous monitoring (see Evidence - Continuous Monitoring)

### Deployment in a Container **(EVIDENCE CRITERIA #i)** 
Our application was deployed using [Docker](https://www.docker.com/) on a Linux - Ubuntu Server 14.04 LTS  (see Evidence - Software Deployment)

### Iterative Approach  **(EVIDENCE CRITERIA #j)** 
We used Scrum as our agile software development methodology (see Evidence - Software Development Approach)

### Installation Instructions **(EVIDENCE CRITERIA #k)** 
The prototype can be installed and ran on Linux - Ubuntu Server 14.04 LTS VM using a Docker web container  (see Evidence - Prototype Installation Instructions)

### Platforms used **(EVIDENCE CRITERIA #l)** 

* Microsoft Visual Studio 2015 Release Candidate or Visual Studio Code
* TeamCity - Continuous Integration
* Ubuntu Server 14.04 LTS - Web Server
* Docker - Web Container 
* Windows Azure - Hosting Environment (Infrastructure as a Service)