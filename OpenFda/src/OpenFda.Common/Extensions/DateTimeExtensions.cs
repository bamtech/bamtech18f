﻿using System;

namespace OpenFda.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToShortDate(this DateTime input)
        {
            return input.ToString("yyyy-MM-dd");
        }

        public static string ToShortDate(this DateTime? input)
        {
            return input == null ? "" : ((DateTime)input).ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// returns the start of the day of the current variable.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static DateTime StartOfDay(this DateTime date)
        {
            var startOfDay = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            return startOfDay;
        }

        /// <summary>
        /// returns the end of the day of the current variable.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static DateTime EndOfDay(this DateTime date)
        {
            var endOfDay = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
            return endOfDay;
        }

        public static string ExtensionToDateTimeNoSecondsString(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd hh:mm tt");
        }

        public static string ExtensionToShortDateString(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
    }
}
