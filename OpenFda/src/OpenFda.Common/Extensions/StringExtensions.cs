﻿//using System.Linq;
//using HtmlAgilityPack;

namespace OpenFda.Common.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Looks for &lt;script&gt;&lt;/script&gt; tags in a string and removes them if they exist
        /// </summary>
        /// <param name="input"></param>
        /// <returns>input with script tags removed (if they exist). Otherwise, returns input untouched</returns>
        public static string RemoveScriptTag(this string input)
        {
            return input;
            // null check
            /*if (string.IsNullOrEmpty(input)) return string.Empty;
            
            // load into html document (part of agility pack)
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(input);

            // remove all nodes that are under a script
            htmlDocument.DocumentNode.Descendants().Where(x => x.Name == "script").ToList().ForEach(f => f.Remove());

            return htmlDocument.DocumentNode.OuterHtml;*/
        }

    }
}