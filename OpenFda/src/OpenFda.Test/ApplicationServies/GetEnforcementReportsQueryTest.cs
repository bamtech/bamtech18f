﻿using System;
using System.Linq;
using OpenFda.ApplicationsServices;
using OpenFda.ApplicationsServices.EnforcementReport;
using OpenFda.Common.Extensions;
using Xunit;

namespace OpenFda.Test.ApplicationServies
{
    public class GetEnforcementReportsQueryTest : TestBase
    {
        [Fact]
        public void TestFoodClassI()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType="Food", Classification = Utilities.FdaClassification.Class1, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r=> r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class1)));
        }


        [Fact]
        public void TestFoodClassII()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType = "Food", Classification = Utilities.FdaClassification.Class2, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r => r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class2)));
        }



        [Fact]
        public void TestFoodClassIII()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType = "Food", Classification = Utilities.FdaClassification.Class3, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r => r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class3)));
        }

        [Fact]
        public void TestDrugsClassI()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType = "Drugs", Classification = Utilities.FdaClassification.Class1, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r => r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class1)));
        }


        [Fact]
        public void TestDrugsClassII()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType = "Drugs", Classification = Utilities.FdaClassification.Class2, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r => r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class2)));
        }



        [Fact]
        public void TestDrugsClassIII()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType = "Drugs", Classification = Utilities.FdaClassification.Class3, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r => r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class3)));
        }


        [Fact]
        public void TestDevicesClassI()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType = "Devices", Classification = Utilities.FdaClassification.Class1, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r => r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class1)));
        }


        [Fact]
        public void TestDevicesClassII()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType = "Devices", Classification = Utilities.FdaClassification.Class2, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r => r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class2)));
        }



        [Fact]
        public void TestDevicesClassIII()
        {
            var startDate = DateTime.Now.AddDays(-90).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var results = Mediator.Send(new GetEnforcementReportsQuery { ProductType = "Devices", Classification = Utilities.FdaClassification.Class3, StartDate = startDate, EndDate = endDate });

            Assert.True(results != null);
            Assert.True(results.Results.Any());
            Assert.False(results.Results.Any(r => r.Classification != Utilities.GetClassificationText(Utilities.FdaClassification.Class3)));
        }


    }
}
