﻿using System;
using OpenFda.ApplicationsServices;
using OpenFda.ApplicationsServices.EnforcementReport;
using OpenFda.Common.Extensions;
using Xunit;

namespace OpenFda.Test.ApplicationServies
{
    public class GetEnforcementClassificationTotalQueryTest : TestBase
    {

        [Fact]
        public void TestFoodClassification1()
        {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassI = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Food", Classification = Utilities.FdaClassification.Class1, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassI != null);
            Assert.False(string.IsNullOrEmpty(resultClassI.Total));
            Assert.True(resultClassI.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class1));
        }

        [Fact]
        public void TestFoodClassification2() {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassII = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Food", Classification = Utilities.FdaClassification.Class2, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassII != null);
            Assert.False(string.IsNullOrEmpty(resultClassII.Total));
            Assert.True(resultClassII.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class2));
        }

        [Fact]
        public void TestFoodClassification3() {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString(); 
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassIII = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Food",Classification = Utilities.FdaClassification.Class3, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassIII != null);
            Assert.False(string.IsNullOrEmpty(resultClassIII.Total));
            Assert.True(resultClassIII.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class3));
        }

        [Fact]
        public void TestDrugsClassification1()
        {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassI = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Drugs", Classification = Utilities.FdaClassification.Class1, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassI != null);
            Assert.False(string.IsNullOrEmpty(resultClassI.Total));
            Assert.True(resultClassI.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class1));
        }

        [Fact]
        public void TestDrugsClassification2()
        {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassII = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Drugs", Classification = Utilities.FdaClassification.Class2, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassII != null);
            Assert.False(string.IsNullOrEmpty(resultClassII.Total));
            Assert.True(resultClassII.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class2));
        }

        [Fact]
        public void TestDrugsClassification3()
        {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassIII = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Drugs", Classification = Utilities.FdaClassification.Class3, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassIII != null);
            Assert.False(string.IsNullOrEmpty(resultClassIII.Total));
            Assert.True(resultClassIII.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class3));
        }

        [Fact]
        public void TestDevicesClassification1()
        {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassI = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Devices", Classification = Utilities.FdaClassification.Class1, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassI != null);
            Assert.False(string.IsNullOrEmpty(resultClassI.Total));
            Assert.True(resultClassI.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class1));
        }

        [Fact]
        public void TestDevicesClassification2()
        {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassII = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Devices", Classification = Utilities.FdaClassification.Class2, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassII != null);
            Assert.False(string.IsNullOrEmpty(resultClassII.Total));
            Assert.True(resultClassII.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class2));
        }

        [Fact]
        public void TestDevicesClassification3()
        {
            var startDate = DateTime.Now.AddDays(-180).ExtensionToShortDateString();
            var endDate = DateTime.Now.ExtensionToShortDateString();
            var resultClassIII = Mediator.Send(new GetEnforcementClassificationTotalQuery { ProductType = "Devices", Classification = Utilities.FdaClassification.Class3, StartDate = startDate, EndDate = endDate });

            Assert.True(resultClassIII != null);
            Assert.False(string.IsNullOrEmpty(resultClassIII.Total));
            Assert.True(resultClassIII.Classfication == Utilities.GetClassificationText(Utilities.FdaClassification.Class3));
        }

    }
}
