﻿using OpenFda.ApplicationsServices.EnforcementReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace OpenFda.Test.ApplicationServies
{
    public class GetEnforcementSearchResultsQueryTest : TestBase
    {
        [Fact]
        public void SearchResultTest()
        {
            var result = Mediator.Send(new GetEnforcementSearchResultsQuery {Phrase = "Whole", Skip=0 });

            Assert.True(result != null);
            Assert.True(result.SearchResults.Any());
        }

    }
}
