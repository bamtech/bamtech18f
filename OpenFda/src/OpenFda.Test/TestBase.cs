﻿using System.Collections.Generic;
using System.Reflection;
using Autofac;
using Autofac.Features.Variance;
using MediatR;
using OpenFda.ApplicationsServices;
using OpenFda.ApplicationsServices.EnforcementReport;

namespace OpenFda.Test
{
    public class TestBase
    {
        private IMediator _mediator;
        public IMediator Mediator { get
            {
                if (_mediator == null)
                {
                    var builder = new ContainerBuilder();
                    builder.RegisterSource(new ContravariantRegistrationSource());
                    builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly).AsImplementedInterfaces();
                    builder.RegisterAssemblyTypes(typeof(GetEnforcementReportQuery).GetTypeInfo().Assembly).AsImplementedInterfaces();
                    builder.Register<SingleInstanceFactory>(ctx =>
                    {
                        var c = ctx.Resolve<IComponentContext>();
                        return t => c.Resolve(t);
                    });
                    builder.Register<MultiInstanceFactory>(ctx =>
                    {
                        var c = ctx.Resolve<IComponentContext>();
                        return t => (IEnumerable<object>)c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
                    });

                    _mediator = builder.Build().Resolve<IMediator>();
                }

                return _mediator;
            } }
    }
}
