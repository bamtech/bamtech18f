﻿using System;
using System.Collections.Generic;
using MediatR;
using Microsoft.AspNet.Mvc;
using OpenFda.ApplicationsServices;
using OpenFda.ApplicationsServices.Models;
using OpenFda.Common.Extensions;
using System.Linq;
using Microsoft.AspNet.Mvc.Rendering;
using OpenFda.ApplicationsServices.EnforcementReport;
using OpenFda.ViewModels;

namespace OpenFda.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;
        private string _startDate = DateTime.Now.AddDays(-89).ExtensionToShortDateString();
        private string _endDate = DateTime.Now.ExtensionToShortDateString();

        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Search([Bind("Search","Skip", "Previous", "Next")] IndexViewModel model)
        {
            if (model.Previous)
                model.Skip -= 10;
            else if (model.Next)
                model.Skip += 10;

            var result = _mediator.Send(new GetEnforcementSearchResultsQuery {Phrase = model.Search, Skip = model.Skip});
            var response = new IndexViewModel
            {
                Results = result.SearchResults,
                TotalResults = result.TotalResults,
                Skip = model.Skip ?? 0,
                Search = model.Search                
            };
            ResetModelState();
            return View(response);
        }

        public IActionResult FoodRecall()
        {
            var productType = "Food";
            var viewmodel = GetRecallViewModel(_startDate, _endDate, productType);

            return View(viewmodel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult FoodRecall([Bind("DateRange", "Skip", "Previous", "Next")] RecallViewModel model)
        {
            var productType = "Food";
            if (model.Previous)
                model.Skip -= 10;
            else if (model.Next)
                model.Skip += 10;

            var skip = model.Skip ?? 0;
            var viewmodel = GetRecallViewModel(model.StartDate.Value.ExtensionToShortDateString(), model.EndDate.Value.ExtensionToShortDateString(), productType, skip);
            return View(viewmodel);
        }

        public IActionResult DrugsRecall()
        {
            var productType = "Drugs";
            var viewmodel = GetRecallViewModel(_startDate, _endDate, productType);

            return View(viewmodel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult DrugsRecall([Bind("DateRange", "Skip", "Previous", "Next")] RecallViewModel model)
        {
            var productType = "Drugs";
            if (model.Previous)
                model.Skip -= 10;
            else if (model.Next)
                model.Skip += 10;

            var skip = model.Skip ?? 0;            
            var viewmodel = GetRecallViewModel(model.StartDate.Value.ExtensionToShortDateString(), model.EndDate.Value.ExtensionToShortDateString(), productType, skip);

            return View(viewmodel);
        }

        public IActionResult DevicesRecall()
        {
            var productType = "Devices";
            var viewmodel = GetRecallViewModel(_startDate, _endDate, productType);

            return View(viewmodel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult DevicesRecall([Bind("DateRange", "Skip", "Previous", "Next")] RecallViewModel model)
        {
            var productType = "Devices";
            if (model.Previous)
                model.Skip -= 10;
            else if (model.Next)
                model.Skip += 10;

            var skip = model.Skip ?? 0;
            var viewmodel = GetRecallViewModel(model.StartDate.Value.ExtensionToShortDateString(), model.EndDate.Value.ExtensionToShortDateString(), productType, skip);

            return View(viewmodel);
        }

        public IActionResult RecallNotification(string id, string productType)
        {
            var viewmodel = _mediator.Send(new GetEnforcementReportQuery
                { Id = id, ProductType = productType }).Results.FirstOrDefault();

            return View(viewmodel);
        }

        private RecallViewModel GetRecallViewModel(string startDate, string endDate, string productType, int skip = 0)
        {
            var results = _mediator.Send(new GetEnforcementReportsQuery { StartDate = startDate, EndDate = endDate, ProductType=productType, Skip = skip});

            var resultClassI =
                _mediator.Send(new GetEnforcementClassificationTotalQuery
                {
                    Classification = Utilities.FdaClassification.Class1, StartDate = startDate, EndDate = endDate, ProductType = productType
                });

            var resultClassII =
                _mediator.Send(new GetEnforcementClassificationTotalQuery
                {
                    Classification = Utilities.FdaClassification.Class2, StartDate = startDate, EndDate = endDate, ProductType = productType
                });

            var resultClassIII =
                _mediator.Send(new GetEnforcementClassificationTotalQuery
                {
                    Classification = Utilities.FdaClassification.Class3, StartDate = startDate, EndDate = endDate, ProductType = productType
                });

            var resultByFirm = _mediator.Send(new GetEnforcementAggregateTotalQuery { GroupingField = "recalling_firm", StartDate = startDate, EndDate = endDate, ProductType = productType });

            var viewmodel = new RecallViewModel
            {
                TotalClassI = resultClassI.Total ?? "0",
                TotalClassII = resultClassII.Total ?? "0",
                TotalClassIII = resultClassIII.Total ?? "0",
                DateRange = startDate + " - " + endDate,
                ProductType = productType,
                Results = results.Results == null ? new List<EnforcementReportItem>() : results.Results.OrderBy(r => r.ReportDate).ToList(),
                TotalByFirm = resultByFirm.Results,
                TotalResults = results.TotalResults,
                Skip = skip
            };
            ResetModelState();
            return viewmodel;
        }

        private IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }

        public DateTime? ExtractStartDate(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                var endOfStartDate = date.IndexOf(" -");
                var dateString = date.Substring(0, endOfStartDate);
                DateTime startDate;
                if (DateTime.TryParse(dateString, out startDate))
                    return startDate;
            }
            return null;
        }

        private DateTime? ExtractEndDate(string date)
        {

            if (!string.IsNullOrEmpty(date))
            {
                var startOfEndDate = date.IndexOf("- ") + 2;
                var dateString = date.Substring(startOfEndDate);
                DateTime endDate;
                if (DateTime.TryParse(dateString, out endDate))
                    return endDate;
            }
            return null;
        }

        private void ParseDateRangeValues(string dateRange)
        {
            if (string.IsNullOrEmpty(dateRange)) return;
            var tempStartDate = Convert.ToDateTime(ExtractStartDate(dateRange));
            var tempEndDate = Convert.ToDateTime(ExtractEndDate(dateRange));
            _startDate = tempStartDate.ExtensionToShortDateString();
            _endDate = tempEndDate.ExtensionToShortDateString();
        }

        private void ResetModelState()
        {
            ModelState.Remove("Skip");
            ModelState.Remove("Previous");
            ModelState.Remove("Next");
            ModelState.Remove("Search");
        }

    }
}
