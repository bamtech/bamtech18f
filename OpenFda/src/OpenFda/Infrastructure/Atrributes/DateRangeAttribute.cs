﻿using System.ComponentModel.DataAnnotations;

namespace OpenFda.Web.Infrastructure.Atrributes
{
    public class DateRangeAttribute : RegularExpressionAttribute
    {
        public DateRangeAttribute()
            : base(@"^(19|20)\d{2}-([1-9]|0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]) - (19|20)\d{2}-([1-9]|0[1-9]|1[0-2])-([1-9]|0[1-9]|1\d|2\d|3[01])$")
        {
        }
    }
}