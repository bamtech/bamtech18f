﻿using System.Collections.Generic;
using MediatR;
using Microsoft.AspNet.Mvc;
using OpenFda.ApplicationsServices.Models;
using OpenFda.ViewModels;

namespace OpenFda.ViewComponents
{
    public class EnforcementReportGraphsViewComponent : ViewComponent
    {
        private readonly IMediator _mediator;

        public EnforcementReportGraphsViewComponent(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IViewComponentResult Invoke(RecallViewModel model)
        {
            return View(model);
        }
    }
}