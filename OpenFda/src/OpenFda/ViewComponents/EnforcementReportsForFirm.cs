﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNet.Mvc;
using OpenFda.ApplicationsServices.EnforcementReport;

namespace OpenFda.ViewComponents
{
    public class EnforcementReportsForFirm : ViewComponent
    {
        private readonly IMediator _mediator;

        public EnforcementReportsForFirm(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IViewComponentResult Invoke(string recallingFirm, string productType)
        {
            var result = _mediator.Send(new GetEnforcementReportsByFirmQuery {RecallingFirm = recallingFirm, ProductType = productType });
            return View(result.Results);
        }
    }
}
