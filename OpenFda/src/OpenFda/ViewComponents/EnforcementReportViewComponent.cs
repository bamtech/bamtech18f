﻿using Microsoft.AspNet.Mvc;
using MediatR;
using System.Linq;
using System.Collections.Generic;
using OpenFda.ViewModels;

namespace OpenFda.ViewComponents
{
    public class EnforcementReportViewComponent : ViewComponent
    {
        private readonly IMediator _mediator;

        public EnforcementReportViewComponent(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IViewComponentResult Invoke(PagedCollectionViewModel model)
        {
            return View(model);
        }

    }
}
