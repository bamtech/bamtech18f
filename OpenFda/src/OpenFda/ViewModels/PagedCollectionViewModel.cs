﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenFda.ApplicationsServices.Models;

namespace OpenFda.ViewModels
{
    public class PagedCollectionViewModel
    {
        public int? Skip { get; set; }
        public bool Previous { get; set; }
        public bool Next { get; set; }
        public int Limit { get { return 10; } }
        public int TotalResults { get; set; }

        public IList<EnforcementReportItem> Results { get; set; }

        public string RangeString
        {
            get
            {
                return string.Format("Showing {0}-{1} of {2} total recalls", Skip + 1, Skip + 1 + Limit, TotalResults);
            }
        }
    }
}
