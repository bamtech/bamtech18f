﻿namespace OpenFda.ViewModels
{
    public class IndexViewModel : PagedCollectionViewModel
    {
        public string Search { get; set; } 
    }
}