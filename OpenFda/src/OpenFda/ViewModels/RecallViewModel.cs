﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;

using OpenFda.Web.Infrastructure.Atrributes;

namespace OpenFda.ViewModels
{
    public class RecallViewModel : PagedCollectionViewModel
    {
        public string TotalClassI { get; set; }
        public string TotalClassII { get; set; }
        public string TotalClassIII { get; set; }
        [Display(Name="Product Type")]
        public string ProductType { get; set; }
        public IList<KeyValuePair<string,int>> TotalByFirm { get; set; }


        [Display(Name = "Reporting Date Range:")]
        [DateRange(ErrorMessage = @"Incorrect format")]
        public string DateRange { get; set; }

        public DateTime? StartDate
        {
            get
            {
                if (!String.IsNullOrEmpty(DateRange))
                {
                    var endOfStartDate = DateRange.IndexOf(" -");
                    var dateString = DateRange.Substring(0, endOfStartDate);
                    DateTime startDate;
                    if (DateTime.TryParse(dateString, out startDate))
                        return startDate;
                }
                return null;
            }
        }

        public DateTime? EndDate
        {
            get
            {
                if (!String.IsNullOrEmpty(DateRange))
                {
                    var startOfEndDate = DateRange.IndexOf("- ") + 2;
                    var dateString = DateRange.Substring(startOfEndDate);
                    DateTime endDate;
                    if (DateTime.TryParse(dateString, out endDate))
                        return endDate;
                }
                return null;
            }

        }

    }
}