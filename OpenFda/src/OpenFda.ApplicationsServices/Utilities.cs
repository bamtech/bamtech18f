﻿using System;
using System.Globalization;
using Newtonsoft.Json.Linq;
using OpenFda.ApplicationsServices.Models;

namespace OpenFda.ApplicationsServices
{
    public static class Utilities
    {

        public const string API_KEY = "bxx8QStC7eegy0dU2a8pPSc80lRX4qdHw9BlVkQ0";
        public const string FOOD_ENFORCEMENT_API_ENDPOINT = "https://api.fda.gov/food/enforcement.json?api_key=" + API_KEY;
        public const string DRUG_ENFORCEMENT_API_ENDPOINT = "https://api.fda.gov/drug/enforcement.json?api_key=" + API_KEY;
        public const string DEVICE_ENFORCEMENT_API_ENDPOINT = "https://api.fda.gov/device/enforcement.json?api_key=" + API_KEY;

        /// <summary>
        /// FdaClassification enum
        /// </summary>
        public enum FdaClassification
        {
            Class1 = 1,
            Class2 = 2,
            Class3 = 3

        }

        /// <summary>
        /// Returns meaning for a particular FdaClassification enum
        /// </summary>
        /// <param name="fdaClassification"></param>
        /// <returns></returns>
        public static string GetClassificationText(FdaClassification fdaClassification)
        {
            switch (fdaClassification)
            {
                case FdaClassification.Class1:
                    return "Class I";
                case FdaClassification.Class2:
                    return "Class II";
                case FdaClassification.Class3:
                    return "Class III";
                default:
                    return "Class I";
            }
        }

        /// <summary>
        /// Return a string value of the integer representing the FdaClassification enum
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static FdaClassification GetClassification(string value)
        {
            switch (value)
            {
                case "1":
                    return FdaClassification.Class1;
                case "2":
                    return FdaClassification.Class2;
                case "3":
                    return FdaClassification.Class3;
                default:
                    return FdaClassification.Class1;
            }
        }

        public static EnforcementReportItem DeserializeEnforcementReportItem(JToken token)
        {
            return new EnforcementReportItem
            {
                Id = (string)token.SelectToken("@id"),
                RecallNumber = (string)token.SelectToken("recall_number"),
                ReasonForRecall = (string)token.SelectToken("reason_for_recall"),
                Status = (string)token.SelectToken("status"),
                DistributionPattern = (string)token.SelectToken("distribution_pattern"),
                ProductQuantity = (string)token.SelectToken("product_quantity"),
                RecallInitiationDate = DateTime.ParseExact((string)token.SelectToken("recall_initiation_date"), "yyyyMMdd", CultureInfo.InvariantCulture),
                State = (string)token.SelectToken("state"),
                EventId = (string)token.SelectToken("event_id"),
                ProductType = (string)token.SelectToken("product_type"),
                ProductDescription = (string)token.SelectToken("product_description"),
                Country = (string)token.SelectToken("country"),
                City = (string)token.SelectToken("city"),
                RecallingFirm = (string)token.SelectToken("recalling_firm"),
                ReportDate = (string)token.SelectToken("report_date"),
                VoluntaryMandated = (string)token.SelectToken("voluntary_mandated"),
                Classification = (string)token.SelectToken("classification"),
                CodeInfo = (string)token.SelectToken("code_info"),
                InitialFirmNotification = (string)token.SelectToken("initial_firm_notification")
            };
        }
    }
}