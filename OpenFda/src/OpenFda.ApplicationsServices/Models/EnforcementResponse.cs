﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Linq;

namespace OpenFda.ApplicationsServices.Models
{
    public class EnforcementResponse
    {
        public EnforcementReportMetaData EnforcementReportMetaData { get; set; }

        public List<EnforcementReportItem> Results { get; set; }

        public EnforcementResponse(string json)
        {
            var jObject = JObject.Parse(json);
            var metaObj = jObject.SelectToken("meta");
            var metaData = new EnforcementReportMetaData
            {
                Disclaimer = (string)metaObj.SelectToken("disclaimer"),
                LastUpdated = (string)metaObj.SelectToken("last_updated"),
                EnforementReportResults = new EnforementReportResults
                {
                    Total = (string)metaObj.SelectToken("results").SelectToken("total")
                }
            };

            var resultsArray = (JArray)jObject.SelectToken("results");
            var results = new List<EnforcementReportItem>();

            foreach (var item in resultsArray)
            {
                results.Add(Utilities.DeserializeEnforcementReportItem(item));
            }
            EnforcementReportMetaData = metaData;
            Results = results;
        }

        
    }
}