using System;
using OpenFda.ApplicationsServices.News;

namespace OpenFda.ApplicationsServices.Models
{
    public class EnforcementReportItem
    {        
        public string Id { get; set; }
        
        public string RecallNumber { get; set; }
        
        public string ReasonForRecall { get; set; }

        public string Status { get; set; }
        
        public string DistributionPattern { get; set; }

        public string ProductQuantity { get; set; }

        public DateTime RecallInitiationDate { get; set; }

        public string State { get; set; }

        public string EventId { get; set; }

        public string ProductType { get; set; }
        
        public string ProductDescription { get; set; }

        public string Country { get; set; }
                
        public string City { get; set; }

        public string RecallingFirm { get; set; }

        public string ReportDate { get; set; }       
         
        public string VoluntaryMandated { get; set; }
        
        public string Classification { get; set; }
        
        public string CodeInfo { get; set; }

        public string InitialFirmNotification { get; set; }

        public NewsResult News { get; set; }
    }
}