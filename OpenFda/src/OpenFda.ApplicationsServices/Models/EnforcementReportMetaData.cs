﻿

namespace OpenFda.ApplicationsServices.Models
{
    public class EnforcementReportMetaData
    {        
        public string Disclaimer { get; set; }
       
        public string LastUpdated { get; set; }
        
        public EnforementReportResults EnforementReportResults { get; set; }
    }

}