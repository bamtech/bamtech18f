﻿using System;
using System.Net.Http;

namespace OpenFda.ApplicationsServices.Infrastructure
{
    public static class RestHelper
    {
        private static string AuthId { get { return ""; } }
        public static T Get<T>(string url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    var response = client.GetAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var json = response.Content.ReadAsStringAsync().Result;
                        return (T)Activator.CreateInstance(typeof(T), new[] { json });
                    }
                }
            }
            catch (Exception Ex) {
                //TODO: Logging
                return Activator.CreateInstance<T>();
            }
            return Activator.CreateInstance<T>();

        }

    }
}
