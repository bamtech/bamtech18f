﻿namespace OpenFda.ApplicationsServices.Infrastructure
{
    public interface IQueryProcessor
    {
        TResult Process<TResult>(IQuery<TResult> query);
    }
}
