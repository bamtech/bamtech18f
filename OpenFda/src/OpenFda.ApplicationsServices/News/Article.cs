﻿namespace OpenFda.ApplicationsServices.News
{
    public class Article
    {
        public string Url { get; set; }
        public string Headline { get; set; }
        public string Publisher { get; set; }
        public string ImageUrl { get; set; }
    }
}
