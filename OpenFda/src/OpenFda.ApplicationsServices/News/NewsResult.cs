﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace OpenFda.ApplicationsServices.News
{
    public class NewsResult
    {
        public NewsResult() { }
        public NewsResult(string json)
        {
            try
            {
                if (string.IsNullOrEmpty(json))
                    return;
                var jObject = JObject.Parse(json);
                var results = (JArray)jObject.SelectToken("responseData").SelectToken("results");
                Articles = new List<Article>();
                foreach (var result in results)
                {
                    Articles.Add(DeserializeArticle(result));
                }                
            }
            catch(Exception ex)
            {

            }
        }

        private Article DeserializeArticle(JToken token)
        {
            var article = new Article
            {
                Url = (string)token.SelectToken("unescapedUrl"),
                Headline = (string)token.SelectToken("titleNoFormatting"),
                Publisher = (string)token.SelectToken("publisher"),
            };

            var image = token.SelectToken("image");
            if (image != null)
                article.ImageUrl = (string)image.SelectToken("tbUrl");

            return article;
        }
        
        public IList<Article> Articles { get;set; }
    }
}
