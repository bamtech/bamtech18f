﻿using MediatR;
using OpenFda.ApplicationsServices.Infrastructure;
using OpenFda.ApplicationsServices.Models;

namespace OpenFda.ApplicationsServices.EnforcementReport
{
    /// <summary>
    /// GetEnforcementClassificationTotalQuery query parameters object
    /// </summary>
    public class GetEnforcementClassificationTotalQuery : IRequest<GetEnforcementClassificationTotalResult>
    {
        public Utilities.FdaClassification Classification { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ProductType { get; set; }
    }

    /// <summary>
    /// GetEnforcementClassificationTotalResult results object
    /// </summary>
    public class GetEnforcementClassificationTotalResult
    {
        public GetEnforcementClassificationTotalResult() { }
        public GetEnforcementClassificationTotalResult(string json)
        {
            var responseObj = new EnforcementResponse(json);
            Total = responseObj.EnforcementReportMetaData.EnforementReportResults.Total;
        }
        public string Classfication { get; set; }
        public string Total { get; set; }
    }

    /// <summary>
    /// GetEnforcementClassificationHandler query request handler
    /// </summary>
    public class GetEnforcementClassificationHandler :
        IRequestHandler<GetEnforcementClassificationTotalQuery, GetEnforcementClassificationTotalResult>
    {
        public GetEnforcementClassificationTotalResult Handle(GetEnforcementClassificationTotalQuery message)
        {
            // Code to get Result by hitting Web API then mapping them to GetDrugEventQueryResult
            var classificationText = Utilities.GetClassificationText(message.Classification);

            string url;

            switch (message.ProductType)
            {
                case "Food":
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT;
                    break;
                case "Drugs":
                    url = Utilities.DRUG_ENFORCEMENT_API_ENDPOINT;
                    break;
                case "Devices":
                    url = Utilities.DEVICE_ENFORCEMENT_API_ENDPOINT;
                    break;
                default:
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT;
                    break;
            }

            url += @"&search=classification:""" + classificationText + "\"+AND+report_date:[" + message.StartDate + "+TO+" + message.EndDate + "]";

            var result = RestHelper.Get<GetEnforcementClassificationTotalResult>(url);
            result.Classfication = classificationText;
            return result;
        }
    }

}
