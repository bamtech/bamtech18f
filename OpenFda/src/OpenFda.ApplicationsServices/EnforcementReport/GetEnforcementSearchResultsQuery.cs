﻿using System.Collections;
using System.Collections.Generic;
using MediatR;
using OpenFda.ApplicationsServices.Infrastructure;
using OpenFda.ApplicationsServices.Models;

namespace OpenFda.ApplicationsServices.EnforcementReport
{
    public class GetEnforcementSearchResultsQuery : IRequest<GetEnforcementSearchResults>
    {
        public string Phrase { get; set; }     
        public int? Skip { get; set; }        
    }

    public class GetEnforcementSearchResults
    {
        public IList<EnforcementReportItem> SearchResults { get; set; }
        public int TotalResults { get; set; }
    }

    public class GetEnforcementSearchResultsQueryHandler :
        IRequestHandler<GetEnforcementSearchResultsQuery, GetEnforcementSearchResults>
    {
        public GetEnforcementSearchResults Handle(GetEnforcementSearchResultsQuery message)
        {
            var result = new GetEnforcementSearchResults
            {
                SearchResults = new List<EnforcementReportItem>()
            };

            var url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT + "&search=product_description:\"" 
                + message.Phrase + "\"&limit=10";


            if (message.Skip != null)
            {
                url += "&skip=" + message.Skip;
            }

            var foodResult = RestHelper.Get<GetEnforcementReportsResult>(url);

            var totalResults = foodResult.TotalResults;

            if (foodResult?.Results != null)
            {
                foreach (var item in foodResult.Results)
                {
                    result.SearchResults.Add(item);
                }
            }

            url = Utilities.DRUG_ENFORCEMENT_API_ENDPOINT + "&search=product_description:\""
                + message.Phrase + "\"&limit=10";

            if (message.Skip != null)
            {
                url += "&skip=" + message.Skip;
            }

            var drugResult = RestHelper.Get<GetEnforcementReportsResult>(url);

            if (drugResult?.Results != null)
            {
                foreach (var item in drugResult.Results)
                {
                    result.SearchResults.Add(item);
                }
            }

            totalResults += drugResult.TotalResults;

            url = Utilities.DEVICE_ENFORCEMENT_API_ENDPOINT + "&search=product_description:\""
                + message.Phrase + "\"&limit=10";


            if (message.Skip != null)
            {
                url += "&skip=" + message.Skip;
            }

            var deviceResult = RestHelper.Get<GetEnforcementReportsResult>(url);

            if (deviceResult?.Results != null)
            {
                foreach (var item in deviceResult.Results)
                {
                    result.SearchResults.Add(item);
                }
            }            
            totalResults += deviceResult.TotalResults;

            result.TotalResults = totalResults;

            return result;
        }
    }
}