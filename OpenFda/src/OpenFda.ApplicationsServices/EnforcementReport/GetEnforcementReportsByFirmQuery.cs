﻿using System.Collections.Generic;
using MediatR;
using OpenFda.ApplicationsServices.Infrastructure;
using OpenFda.ApplicationsServices.Models;

namespace OpenFda.ApplicationsServices.EnforcementReport
{
    public class GetEnforcementReportsByFirmQuery : IRequest<GetEnforcementReportsByFirmResult>
    {
        public string RecallingFirm { get; set; }
        public string ProductType { get; set; }
    }

    public class GetEnforcementReportsByFirmResult
    {
        public GetEnforcementReportsByFirmResult() { }
        public GetEnforcementReportsByFirmResult(string json)
        {
            var responseObj = new EnforcementResponse(json);
            Results = responseObj.Results;
        }
        public List<EnforcementReportItem> Results;
    }

    public class GetEnforcementReportsByFirmQueryHandler :
        IRequestHandler<GetEnforcementReportsByFirmQuery, GetEnforcementReportsByFirmResult>
    {
        public GetEnforcementReportsByFirmResult Handle(GetEnforcementReportsByFirmQuery message)
        {
            string url;

            switch (message.ProductType)
            {
                case "Food":
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT;
                    break;
                case "Drugs":
                    url = Utilities.DRUG_ENFORCEMENT_API_ENDPOINT;
                    break;
                case "Devices":
                    url = Utilities.DEVICE_ENFORCEMENT_API_ENDPOINT;
                    break;
                default:
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT;
                    break;
            }

            url += "&search=recalling_firm:\"" + message.RecallingFirm + "\"&limit=5";

            return RestHelper.Get<GetEnforcementReportsByFirmResult>(url);

        }
    }
}