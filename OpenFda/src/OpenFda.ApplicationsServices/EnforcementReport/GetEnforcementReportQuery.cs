﻿using System.Collections.Generic;
using System.Linq;
using MediatR;
using OpenFda.ApplicationsServices.Infrastructure;
using OpenFda.ApplicationsServices.Models;
using OpenFda.ApplicationsServices.News;

namespace OpenFda.ApplicationsServices.EnforcementReport
{
    /// <summary>
    /// GetEnforcementReportQuery query parameter object
    /// </summary>
    public class GetEnforcementReportQuery : IRequest<GetEnforcementReportResult>
    {
        public string Id { get; set; }
        public string ProductType { get; set; }
    }

    /// <summary>
    /// GetEnforcementReportResult results object
    /// </summary>
    public class GetEnforcementReportResult
    {
        public GetEnforcementReportResult() { }
        public GetEnforcementReportResult(string json)
        {
            var responseObj = new EnforcementResponse(json);
            Results = responseObj.Results;
        }
        public List<EnforcementReportItem> Results { get; set; }

    }

    /// <summary>
    /// GetEnforcementReportHandler query request handler
    /// </summary>
    public class GetEnforcementReportHandler :
        IRequestHandler<GetEnforcementReportQuery, GetEnforcementReportResult>
    {
        public GetEnforcementReportResult Handle(GetEnforcementReportQuery message)
        {
            // Code to get Result by hitting Web API then mapping them to GetDrugEventQueryResult

            string url;

            switch (message.ProductType)
            {
                case "Food":
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT + "&search=" + message.Id + "&limit=1";
                    break;
                case "Drugs":
                    url = Utilities.DRUG_ENFORCEMENT_API_ENDPOINT + "&search=" + message.Id + "&limit=1";
                    break;
                case "Devices":
                    url = Utilities.DEVICE_ENFORCEMENT_API_ENDPOINT + "&search=" + message.Id + "&limit=1";
                    break;
                default:
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT + "&search=" + message.Id + "&limit=1";
                    break;
            }

            var result = RestHelper.Get<GetEnforcementReportResult>(url);
            if (result.Results.Any())
            {
                var item = result.Results.FirstOrDefault();
                url = "https://ajax.googleapis.com/ajax/services/search/news?v=1.0&q="+ System.Uri.EscapeDataString(item.RecallingFirm + " + recalls");
                item.News = RestHelper.Get<NewsResult>(url);
            }

            return result;
        }
    }
}
