﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using MediatR;
using OpenFda.ApplicationsServices.Infrastructure;
using OpenFda.ApplicationsServices.Models;

namespace OpenFda.ApplicationsServices.EnforcementReport
{

    /// <summary>
    /// GetEnforcementReportsQuery query parameters object
    /// </summary>
    public class GetEnforcementReportsQuery : IRequest<GetEnforcementReportsResult>
    {
        public Utilities.FdaClassification? Classification { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ProductType { get; set; }

        public int? Skip { get; set; }        
    }

    /// <summary>
    /// GetEnforcementReportsResult result object
    /// </summary>
    public class GetEnforcementReportsResult
    {
        public GetEnforcementReportsResult() { }
        public GetEnforcementReportsResult(string json)
        {
            var responseObj = new EnforcementResponse(json);
            try
            {
                var jObject = JObject.Parse(json);
                var totalResults = (int)jObject.SelectToken("meta").SelectToken("results").SelectToken("total");
                TotalResults = totalResults;
            }
            catch { }
            Results = responseObj.Results;
        }
        public List<EnforcementReportItem> Results { get; set; }

        public int TotalResults { get; set; }
    }

    /// <summary>
    /// GetEnforcementReportsQueryHandler query reqeust handler
    /// </summary>
    public class GetEnforcementReportsQueryHandler :
        IRequestHandler<GetEnforcementReportsQuery, GetEnforcementReportsResult>
    {
        public GetEnforcementReportsResult Handle(GetEnforcementReportsQuery message)
        {
            // Code to get Result by hitting Web API then mapping them to GetDrugEventQueryResult
            string url;

            switch (message.ProductType)
            {
                case "Food":
                   url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT + "&search=";
                    break;
                case "Drugs":
                    url = Utilities.DRUG_ENFORCEMENT_API_ENDPOINT + "&search=";
                    break;
                case "Devices":
                    url = Utilities.DEVICE_ENFORCEMENT_API_ENDPOINT + "&search=";
                    break;
                default:
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT + "&search=";
                    break;
            }

 

            if (message.Classification.HasValue)
            {
                url += "classification:\"" + Utilities.GetClassificationText(message.Classification.Value) + "\"+AND+";
            }

            if (message.StartDate != null && message.EndDate != null)
            {
                url += "report_date:[" + message.StartDate + "+TO+" + message.EndDate + "]";

            }
            if (message.Skip != null)
            {
                url += "&skip=" + message.Skip;
            }
            url += "&limit=10";


            return RestHelper.Get<GetEnforcementReportsResult>(url);
        }
    }
}
