﻿using System.Collections.Generic;
using MediatR;
using Newtonsoft.Json.Linq;
using OpenFda.ApplicationsServices.Infrastructure;

namespace OpenFda.ApplicationsServices.EnforcementReport
{
    /// <summary>
    /// GetEnforcementAggregateTotalQuery query parameters object
    /// </summary>
    public class GetEnforcementAggregateTotalQuery : IRequest<GetEnforcementAggregateTotalResult>
    {
        public string GroupingField { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ProductType { get; set; }
    }

    /// <summary>
    /// GetEnforcementAggregateTotalResult result object
    /// </summary>
    public class GetEnforcementAggregateTotalResult
    {
        public GetEnforcementAggregateTotalResult() { }
        public GetEnforcementAggregateTotalResult(string json)
        {
            var jObject = JObject.Parse(json);
            var resultsArray = (JArray)jObject.SelectToken("results");
            Results = new List<KeyValuePair<string, int>>();
            foreach(var item in resultsArray)
            {
                Results.Add(new KeyValuePair<string,int>((string)item.SelectToken("term"), (int)item.SelectToken("count")));
            }
        }                
        public string GroupingField { get; set; }
        public IList<KeyValuePair<string,int>> Results { get; set; }
    }

    /// <summary>
    /// GetEnforcementAggregateTotalQuery query request
    /// </summary>
    public class GetEnforcementAggregateTotalHandler :
        IRequestHandler<GetEnforcementAggregateTotalQuery, GetEnforcementAggregateTotalResult>
    {
        public GetEnforcementAggregateTotalResult Handle(GetEnforcementAggregateTotalQuery message)
        {
            // Code to get Result by hitting Web API then mapping them to GetDrugEventQueryResult            
            string url;

            switch (message.ProductType)
            {
                case "Food":
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT;
                    break;
                case "Drugs":
                    url = Utilities.DRUG_ENFORCEMENT_API_ENDPOINT;
                    break;
                case "Devices":
                    url = Utilities.DEVICE_ENFORCEMENT_API_ENDPOINT;
                    break;
                default:
                    url = Utilities.FOOD_ENFORCEMENT_API_ENDPOINT;
                    break;
            }

            url +=  @"&search=report_date:[" + message.StartDate + "+TO+" + message.EndDate + "]&limit=5&count="+message.GroupingField + ".exact";

            var result = RestHelper.Get<GetEnforcementAggregateTotalResult>(url);
            result.GroupingField = message.GroupingField;
            return result;
        }
    }

}
